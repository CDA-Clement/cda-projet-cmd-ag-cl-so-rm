package afpa.console.exception;

public class CommandException extends Exception {
	private static final long serialVersionUID = 1L;

	public CommandException(String msg) {
		super(msg);
	}
}
