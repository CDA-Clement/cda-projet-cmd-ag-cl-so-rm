package afpa.console.command;

import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;

public class CommandeIsprime extends AbstractCommandeAvecParam implements IHistoriqueCommand{
	public static final String CMD = "isprime";
	private static final String DESC = "Afficher si un nombre est premier ou pas";
	
	
	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}

	public CommandeIsprime(String c) {
		super(c, CMD);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		boolean prime= true;
		
		for(int d=2;d < Double.parseDouble(this.parameter); d++) {
			if(Double.parseDouble(this.parameter)%d==0) { 
				prime = false;
				
				
			}
			
		}
		if(prime==true) {
			System.out.println("YES");
		}else
			System.out.println("NO");
		
		
		
		}

}
