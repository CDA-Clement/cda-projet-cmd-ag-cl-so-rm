package afpa.console.command;
import java.io.File;

import afpa.console.command.interfaces.IHistoriqueCommand;
class CommandeDirng extends CommandeDir {
    public static final String CMD = "dirng";
    private static final String DESC = "Affiche la liste et le nombre de fichiers et de sous-répertoires d'un répertoire";
    static {
        CommandeFactory.addCommandeDesc(CMD, DESC);
    }
    public CommandeDirng(String param, String cmdDirng) {
        super(param, cmdDirng);
    }
    public static void chargerStaticPortion() {
    }
    @Override
    public String getName() {
        return CMD;
    }
    @Override
    public void run() {
        int cmptF = 0;
        int cmptD = 0;
        super.run();
        File file1 = new File(this.parameter);
        if (DOUBLE_POINTS.equals(this.parameter)) {
            String str = CommandeFactory.CURRENT_FILE.getParent();
            File file2 = new File(str);
            final File[] filesList = file2.listFiles();
            for (final File childFile : filesList) {
                if (childFile.isDirectory()) {
                    cmptD++;
                } else {
                    cmptF++;
                }
            }
        } else if (file1.isDirectory()) {
            final File[] filesList = file1.listFiles();
            for (final File childFile : filesList) {
                if (childFile.isDirectory()) {
                    cmptD++;
                } else {
                    cmptF++;
                }
            }
        } else {
            final File[] filesList = CommandeFactory.CURRENT_FILE.listFiles();
            for (final File childFile : filesList) {
                if (childFile.isDirectory()) {
                    cmptD++;
                } else {
                    cmptF++;
                }
            }
        }
        System.out.println(cmptF + " fichiers");
        System.out.println(cmptD + " repertoires");
    }
}