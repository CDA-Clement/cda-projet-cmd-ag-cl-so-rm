

package afpa.console.command;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import afpa.console.command.interfaces.ICommand;
import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;
import afpa.console.hist.EntreeCmdHist;

public class CommandeFactory  {



	public static File CURRENT_FILE = new File(System.getProperty("user.dir"));

	private static final List<EntreeCmdHist> COMMANDES_HIST_LIST = new ArrayList<>();

	private static final Map<String, String> COMMANDES_LIST_DESC = new HashMap<>();

	static {
		Now.chargerStaticPortion();
		CommandeCat.chargerStaticPortion();
		CommandeCd.chargerStaticPortion();
		CommandeCrd.chargerStaticPortion();
		CommandeCrf.chargerStaticPortion();
		CommandeCopy.chargerStaticPortion();
		CommandeDir.chargerStaticPortion();
		CommandeDirng.chargerStaticPortion();
		CommandeExit.chargerStaticPortion();
		CommandeFin.chargerStaticPortion();
		CommandeFind.chargerStaticPortion();
		CommandeGetvars.chargerStaticPortion();
		CommandeHelp.chargerStaticPortion();
		CommandeHist.chargerStaticPortion();
		CommandeHistclear.chargerStaticPortion();
		CommandeIsprime.chargerStaticPortion();
		CommandePwd.chargerStaticPortion();
		CommandeQuit.chargerStaticPortion();
		CommandeRiver.chargerStaticPortion();

	}

	public static void addCommandeDesc(String cmd, String desc) {

		COMMANDES_LIST_DESC.put(cmd, desc);
	}

	public static ICommand create(String cmd) {


		ICommand theCommand = null;

		if (cmd.length() == 0) {

			theCommand = new CommandeVide();

			// COMMAND PWD
		} else if (CommandePwd.CMD.equalsIgnoreCase(cmd)) {

			theCommand = new CommandePwd();

			// COMMAND CRD
		
		}else if (cmd.toLowerCase().startsWith(Now.CMD + " ")) {

					theCommand = new Now(cmd);

		} else if (cmd.toLowerCase().startsWith(CommandeCrd.CMD + " ")) {

			theCommand = new CommandeCrd(cmd);

			// COMMAND CAT
		} else if (cmd.toLowerCase().startsWith(CommandeCat.CMD + " ")) {

			theCommand = new CommandeCat(cmd);

			//COMMAND CRF
		} else if (cmd.toLowerCase().startsWith(CommandeCrf.CMD + " ")) {

			theCommand = new CommandeCrf(cmd);

			// COMMAND RIVER
		} else if (cmd.toLowerCase().startsWith(CommandeRiver.CMD + " ")) {

			theCommand = new CommandeRiver(cmd);

			// COMMAND HIST
		} else if (CommandeHist.CMD.equalsIgnoreCase(cmd)) {

			theCommand = new CommandeHist(COMMANDES_HIST_LIST);

			//COMMAND EXIT
		} else if (CommandeExit.CMD.equalsIgnoreCase(cmd)) {

			theCommand = new CommandeExit();

			// COMMAND ISPRIME
		} else if(cmd.toLowerCase().startsWith(CommandeIsprime.CMD + " ")) {

			theCommand = new CommandeIsprime(cmd);

			// COMMAND DIR 
		} else if (cmd.toLowerCase().startsWith(CommandeDir.CMD + " ")) {

			theCommand = new CommandeDir(cmd);

		} else if (CommandeDir.CMD.equalsIgnoreCase(cmd)) {

			theCommand = new CommandeDir(cmd);

			// COMMAND CD
		} else if (cmd.toLowerCase().startsWith(CommandeCd.CMD + " ")) {

			theCommand = new CommandeCd(cmd);

			// COMMAND FIN
		} else if (CommandeFin.CMD.equalsIgnoreCase(cmd)) {

			theCommand = new CommandeFin();
		} else if (Now.CMD.equalsIgnoreCase(cmd)) {

			theCommand = new Now(cmd);
			// COMMAND HISTCLEAR
		} else if (CommandeHistclear.CMD.equalsIgnoreCase(cmd)) {

			theCommand = new CommandeHistclear(COMMANDES_HIST_LIST);

			// COMMAND DIRNG
		} else if (cmd.toLowerCase().startsWith(CommandeDirng.CMD + " ")) {
			theCommand = new CommandeDirng(cmd, CommandeDirng.CMD);
			
		} else if (CommandeDirng.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandeDirng(cmd, CommandeDirng.CMD);

			// COMMAND HELP
		} else if (CommandeHelp.CMD.equalsIgnoreCase(cmd)) {

			theCommand = new CommandeHelp();

			// COMMAND QUIT
		} else if (CommandeQuit.CMD.equalsIgnoreCase(cmd)) {
			theCommand = new CommandeQuit();

			// COMMAND COPY
		} else if (cmd.toLowerCase().startsWith(CommandeCopy.CMD + " ")) {

			theCommand = new CommandeCopy(cmd);

			// COMMAND FIND
		} else if (cmd.toLowerCase().startsWith(CommandeFind.CMD + " ")) {

			theCommand = new CommandeFind(cmd);
			
//		} else if (CommandeFind.CMD.equalsIgnoreCase(cmd)) {
//			theCommand = new CommandeFind(cmd);

			// COMMAND GETVARS
		} else if (cmd.toLowerCase().startsWith(CommandeGetvars.CMD + " ")) {

			theCommand = new CommandeGetvars(cmd);	

			// COMMAND INTROUVABLE
		} else {

			theCommand = new CommandeIntrouvable();
		}

		if (theCommand != null && theCommand instanceof IHistoriqueCommand) {
	           COMMANDES_HIST_LIST.add(new EntreeCmdHist(cmd));
	           if(COMMANDES_HIST_LIST.size()==11) {
	               COMMANDES_HIST_LIST.remove(0);
	           }
	       }
		return theCommand;
	}

	public static Map<String, String> getCommandesList() {
		return new HashMap<>(COMMANDES_LIST_DESC);
	}


}
