package afpa.console.command;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;

public class CommandeCat extends AbstractCommandeAvecParam implements IHistoriqueCommand{
	public static final String CMD = "cat";
	private static final String DESC = "Lire un fichier";

	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}

	public CommandeCat(String c) {
		super(c, CMD);
		
	}
 
	@Override
	public String getName() {
		
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		File directory= new File(CommandeFactory.CURRENT_FILE.getPath());
        File[] content = directory.listFiles();
        for (File file : content) {
            if(file.isFile()) {
                if(file.getName().equals(this.parameter)) {
                    BufferedReader br = null;
                    try {
                        br = new BufferedReader(new FileReader(CommandeFactory.CURRENT_FILE.getPath()+"\\"+this.parameter));
                        String line;
                        while ((line = br.readLine()) != null) {
                            System.out.println(line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            br.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    break; 
                }
            }
        }
    }
		
	}
