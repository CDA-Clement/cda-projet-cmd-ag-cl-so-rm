package afpa.console.command;

import java.io.File;
import java.nio.file.Files;

import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;

public class CommandeCopy extends AbstractCommandeAvecParam implements IHistoriqueCommand{
	public static final String CMD = "copy";
	private static final String DESC = "Copier Coller un fichier";


	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}

	public CommandeCopy(String c) {
		super(c, CMD);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		File originalFile;
		File newFile;
		boolean found = false;
		boolean found1 = true;
		String S1 = this.parameter.substring(0, this.parameter.indexOf(' '));
		S1 = S1.trim();
		String S2 = this.parameter.substring(this.parameter.indexOf(' '), this.parameter.length());
		S2 = S2.trim();    
		originalFile = new File(CommandeFactory.CURRENT_FILE +"//"+ S1);
		newFile = new File(CommandeFactory.CURRENT_FILE +"//"+ S2 ); // l'erreur etait ici, on ne faisait pas d'initialisation de path, donc par defaut il le mettait a la fin du chemin.
		File directory= new File(CommandeFactory.CURRENT_FILE.getPath());
		File[] content = directory.listFiles();
		for (File file : content) {
			if(file.isFile()) {
				if(file.getName().equals(S1)) {
					found = true;
					break;
				}
			}
		}
		File directory1= new File(CommandeFactory.CURRENT_FILE.getPath());
		File[] content1 = directory.listFiles();
		for (File file1 : content) {
			if(file1.isFile()) {
				if(file1.getName().equals(S2)) {
					found1 = false;
					break;
				}
			}
		}
		if(found == true && found1==true) {
			try {
				Files.copy(originalFile.toPath(), newFile.toPath());
			}
			catch(Exception e) {
			}
		}
		if(found==true && found1==true) {
			System.out.println("file copied successfully");
		}else {
			if(found1==false) {
				System.out.println("the file destination you want to copy already exist");
			}
			if(found==false) {
				System.out.println("the file you want to copy does not exist");
			}
		}
		found=false;
		found1=true;
	}
}