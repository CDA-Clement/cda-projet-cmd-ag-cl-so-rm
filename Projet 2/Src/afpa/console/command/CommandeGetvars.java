package afpa.console.command;

import java.util.Map;

import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;

public class CommandeGetvars extends AbstractCommandeAvecParam implements IHistoriqueCommand{
	public static final String CMD = "getvars";
	private static final String DESC = "Affiche les variables";
	static Map<Object, Object> variable = System.getProperties();
	
	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}
	public CommandeGetvars(String c) {
		super(c, CMD);
		// TODO Auto-generated constructor stub
	}
	public static void chargerStaticPortion() {
	}
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		Map<String, String> variable = System.getenv();
        for (Map.Entry<String,String> entry : variable.entrySet()) {
            System.out.println(String.format("%-60s",entry.getKey())+String.format("|   %10s",entry.getValue()));
            
        }
        System.out.println();
    
		
	}

}
