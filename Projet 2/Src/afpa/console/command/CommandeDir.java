package afpa.console.command;
import java.io.File;

import afpa.console.command.interfaces.IHistoriqueCommand;
class CommandeDir extends AbstractCommandeAvecParam {
    public static final String CMD = "dir";
    private static final String DESC = "Affiche la liste des fichiers et des sous-répertoires d'un répertoire";
    public static final String DOUBLE_POINTS = "..";
    static {
        CommandeFactory.addCommandeDesc(CMD, DESC);
    }
    public CommandeDir(String cmd2) {
        super(cmd2, CMD);
    }
    
    public CommandeDir(String cmd2, String cmdDirng) {
        super(cmd2, cmdDirng);
    }
    public static void chargerStaticPortion() {
    }
    @Override
    public String getName() {
        return CMD;
    }
    @Override
    public void run() {
        File file1 = new File(this.parameter);
        if (DOUBLE_POINTS.equals(this.parameter)) {
            String str = CommandeFactory.CURRENT_FILE.getParent();
            file1 = new File(str);
            final File[] filesList = file1.listFiles();
            for (final File childFile : filesList) {
                System.out.println((childFile.isDirectory() ? "<DIR> " : "      ") + childFile.getName());
            }
        } else if (file1.isDirectory()) {
            final File[] filesList = file1.listFiles();
            for (final File childFile : filesList) {
                System.out.println((childFile.isDirectory() ? "<DIR> " : "      ") + childFile.getName());
            }
        } else {
            final File[] filesList = CommandeFactory.CURRENT_FILE.listFiles();
            for (final File childFile : filesList) {
                System.out.println((childFile.isDirectory() ? "<DIR> " : "      ") + childFile.getName());
            }
        }
    }
}
