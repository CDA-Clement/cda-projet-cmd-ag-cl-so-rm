package afpa.console.command;

import java.io.File;

import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;

public class CommandeCrd extends AbstractCommandeAvecParam implements IHistoriqueCommand{
	public static final String CMD = "crd";
	private static final String DESC = "Cr�ation d'un r�pertoire dans le r�pertoire en cours.";
	
	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}
	public CommandeCrd(String c) {
		super(c, CMD);
		// TODO Auto-generated constructor stub
	}
	public static void chargerStaticPortion() {
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		 File fichier = new File(CommandeFactory.CURRENT_FILE +"\\" + this.parameter);
	       fichier.mkdir();
	       System.out.println("Directory successfully created");
	   }

}
