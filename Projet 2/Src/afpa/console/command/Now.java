package afpa.console.command;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter.DEFAULT;

import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;

public class Now extends AbstractCommandeAvecParam implements IHistoriqueCommand{
	public static final String CMD = "now";
	private static final String DESC = "afficher l heure et date";
	private static final SimpleDateFormat SDF2 = new SimpleDateFormat("dd/MM/yy HH:mm:ss");

	private static final SimpleDateFormat SDF3 = new SimpleDateFormat("HH:mm:ss");
	private static final SimpleDateFormat SDF4 = new SimpleDateFormat("dd/MM/yy");

	private final Date dateCreation;
	public Now(String c) {
		super(c, CMD);
		this.dateCreation = new Date();
	}

	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}
	public static void chargerStaticPortion() {
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return CMD;
	}
	@Override
	public void run() throws CommandException {
		
		if(this.parameter.equals("-dt")||this.parameter.equals("-d -t")||this.parameter.equals("-t -d")||this.parameter.equals("-td")||this.CMD.equals(CMD)) {
			System.out.println( SDF2.format(this.dateCreation));

		}else if(this.parameter.equals("-d")) {
			System.out.println( SDF4.format(this.dateCreation));
		}else if(this.parameter.equals("-t")) {
			System.out.println(SDF3.format(this.dateCreation));
		
		}
		

	}

}