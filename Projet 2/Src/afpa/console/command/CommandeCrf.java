package afpa.console.command;

import java.io.File;
import java.io.IOException;

import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;

public class CommandeCrf extends AbstractCommandeAvecParam implements IHistoriqueCommand{
	public static final String CMD = "crf";
	private static final String DESC = "Cr�ation d'un fichier dans le r�pertoire en cours";
	
	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}
	public CommandeCrf(String c) {
		super(c, CMD);
		// TODO Auto-generated constructor stub
	}
	public static void chargerStaticPortion() {
	}
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		   File fichier = new File(CommandeFactory.CURRENT_FILE+"\\"+this.parameter) ;
		    try {
				fichier.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    System.out.println("File successfully created");
		
	}

}
