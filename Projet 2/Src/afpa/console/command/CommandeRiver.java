package afpa.console.command;

import afpa.console.command.interfaces.IHistoriqueCommand;
import afpa.console.exception.CommandException;

public class CommandeRiver extends AbstractCommandeAvecParam implements IHistoriqueCommand{
	public static final String CMD = "river";
	private static final String DESC = "Affiche le point de rencontre de 2 Digits Rivers";
	
	static {
		CommandeFactory.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}

	public CommandeRiver(String c) {
		super(c, CMD);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return CMD;
	}

	@Override
	public void run() throws CommandException {
		boolean max = false;
		String param1 = this.parameter.substring(0, this.parameter.indexOf(' '));
		String param2 = this.parameter.substring(this.parameter.indexOf(' ')+1, this.parameter.length());
		int r1= Integer.parseInt(param1);
		int r2 = Integer.parseInt(param2);
		int somme =0;
		if(r1>r2) {
			int temp =0;
			temp=r1;
			r1=r2;
			r2=temp;
		}
		String nombreString = Integer.toString(r1);
		
		while(r1!=r2) {
			if(r1>r2) {
				int temp =0;
				temp=r1;
				r1=r2;
				r2=temp;	
				String nombreString2 = Integer.toString(r1);
				nombreString=nombreString2;
			}
			for (int i = 0; i < nombreString.length(); i++) {
				
				if(Character.isDigit(nombreString.charAt(i))) {
					somme+= (nombreString.charAt(i)-48);
				}

				
			}

			r1+=somme;
			if(r1<0) {
				max = true;
				
				break;
				
			}
			somme =0;
			String nombreString1 = Integer.toString(r1);
			nombreString=nombreString1;

		}
		if(max == true) {
			System.out.println("you've reached the limit of integer");
		}else {
			System.out.println(r1+"\n");
		}
		
	}

}
