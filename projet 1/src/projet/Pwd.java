package projet;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Map.Entry;

public class Pwd {
	private static String s;
	public Pwd () {
		if(Getvars.variable.containsKey("cdi.default.folder") && Files.isDirectory(Paths.get((String)Getvars.variable.get("cdi.default.folder")))) {
			
		s = (String) Getvars.variable.get("cdi.default.folder");
		}else {
			s= System.getProperty("user.dir");	
		}
		
	}
	
	
	public static void cd() {
		Pwd p = new Pwd();
		p.setS(s);
	}
	public static String getS() {
		return s;
	}
	public static void setS(String s1) {
		s = s1;
	}
	

}
