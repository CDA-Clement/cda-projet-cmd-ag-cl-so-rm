package projet;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Dirng implements Methodspp {
	
	public void displayContent() {
		int i=0;
		int j=0;
		Dir d = new Dir();
		File directory= new File(d.pwd());
		File[] content = directory.listFiles();
		for (File file : content) {
			if(file.isFile()) {
				i++;
				System.out.println("      "+file.getName());
			}
			else if (file.isDirectory()) {
				j++;
				System.out.println("<DIR> "+ file.getName());
			}
		}
		System.out.println(i+" files");
		System.out.println(j+" Directories\n");
	}

	public void help() {
		System.out.println("list of command available for Dir");

	}

	@Override

	public String pwd() {
		String s = System.getProperty("user.dir");
		return s;

	}

	@Override
	public String date() {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss yyyy-MM-dd");
		String formatDateTime = now.format(formatter);
		return formatDateTime;
	}


}
