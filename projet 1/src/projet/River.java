package projet;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import projet.Methodspp;

public class River implements Methodspp {

	public static int river(int a, int b ) {
		boolean max = false;
		int r1 = a;
		int r2 = b;
		int somme =0;
		if(r1>r2) {
			int temp =0;
			temp=r1;
			r1=r2;
			r2=temp;
		}
		String nombreString = Integer.toString(r1);
		
		while(r1!=r2) {
			if(r1>r2) {
				int temp =0;
				temp=r1;
				r1=r2;
				r2=temp;	
				String nombreString2 = Integer.toString(r1);
				nombreString=nombreString2;
			}
			for (int i = 0; i < nombreString.length(); i++) {
				
				if(Character.isDigit(nombreString.charAt(i))) {
					somme+= (nombreString.charAt(i)-48);
				}

				
			}

			r1+=somme;
			if(r1<0) {
				max = true;
				
				break;
				
			}
			somme =0;
			String nombreString1 = Integer.toString(r1);
			nombreString=nombreString1;

		}
		if(max == true) {
			System.out.println("you've reached the limit of integer");
		}else {
			System.out.println(r1+"\n");
		}
		return r1;
		
	}


	@Override
	public void help() {
		
		System.out.println("\n					list of command available for River");
		System.out.println("\nEXIT			: quit River");
		System.out.println("\nHELP 			: Display all the commands available");
		System.out.println("\nPWD 			: Display the line utility for printing the current working directory ");
		System.out.println("\nParamaters: : you need to enter an integer");
	}

	@Override
	public String pwd() {
		String s = System.getProperty("user.dir");
		return s;
	}


	@Override
	public String date() {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss yyyy-MM-dd");
		String formatDateTime = now.format(formatter);
		return formatDateTime;
	}
}
