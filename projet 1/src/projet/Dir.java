package projet;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Dir implements Methodspp {

	public Dir() {
		
	}

	public void displayContent() {
		Dir d = new Dir();
		
		File directory= new File(Pwd.getS());
		File[] content = directory.listFiles();
		for (File file : content) {
			if(file.isFile()) {
				System.out.println("      "+file.getName());
			}
			else if (file.isDirectory()) {
				System.out.println("<DIR> "+ file.getName());
			}
		}
		System.out.println();
	}













	@Override
	public void help() {
		System.out.println("list of command available for Dir");

	}

	@Override

	public String pwd() {
		String s = System.getProperty("user.dir");
		return s;

	}

	@Override
	public String date() {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss yyyy-MM-dd");
		String formatDateTime = now.format(formatter);
		return formatDateTime;
	}


}
