package projet;

import java.io.File;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Cd implements Methodspp {

	public Cd() {
	}

	public boolean changeDirectory(String rivTab) {
		boolean found = false;
		Dir d = new Dir();

		File directory= new File(Pwd.getS());
		File[] content = directory.listFiles();
		for (File file : content) {
			found = false;
			if(rivTab.equalsIgnoreCase("..")) {
				try {
				Pwd.setS(file.toPath().getParent().getParent().toString());
			break;
			}
				catch (Exception e) {
					Pwd.setS(file.toPath().getRoot().toString());
				}
			}
			else if(file.getName().equalsIgnoreCase(rivTab)) {
				if(file.isDirectory()) {
					Pwd.setS(file.toPath()+"\\");
					break;	
				}else {
					System.out.println("you can't go to a file");
				}
				
				
			} else found = true;


		}
		if(found == true){
			System.out.println("the specified command is invalid");
		}
		return found;

	}

	@Override
	public void help() {
		System.out.println("list of command available for Dir");

	}

	@Override

	public String pwd() {
		String s = System.getProperty("user.dir");
		return s;

	}

	@Override
	public String date() {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss yyyy-MM-dd");
		String formatDateTime = now.format(formatter);
		return formatDateTime;
	}

}
