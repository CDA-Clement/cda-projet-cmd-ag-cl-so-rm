package projet;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;

public class Menu implements Methodspp {
	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		String action = "";
		ArrayList<String> list = new ArrayList<String>();
		Menu m = new Menu();
		Pwd p = new Pwd();
		boolean run = false;
		System.out.println(
				"####################################################################################################################");
		System.out.println(
				"####################################################################################################################");
		ASCIIArtGenerator.ART_SIZE_MEDIUM("   TERMINAL");
		System.out.println(
				"####################################################################################################################");
		System.out.println(
				"####################################################################################################################");
		System.out.println();
		System.out.println();

		while (run == false) {
			if (list.size() == 11) {
				list.remove(0);
			}
			System.out.println(p.getS());
			System.out.print("۞ ");
			action = sc.nextLine();
			//action = action.toUpperCase();
			String[] rivTab= action.split(" ") ;
			rivTab[0]= rivTab[0].toUpperCase();


			switch (rivTab[0]) {
			case "HELP":
				String history = action + " : " + m.date();
				//	list.add(history);
				m.help();

				continue;
				
			case "CRF":
				try {
				String history13 = action + " : " + m.date();
					list.add(history13);
				Crf.crf(rivTab[1]);}
				catch (Exception e) {
					System.out.println("Ooops something went wrong please check your paramater");
				}

				continue;
				
			case "CRD":
				try {
				String history14 = action + " : " + m.date();
					list.add(history14);
				Crd.crd(rivTab[1]);}
				catch(Exception e) {
					System.out.println("Ooops something went wrong please check your paramater");
				}

				continue;

			case "PWD":
				//System.out.println(m.pwd()+ "\\pwd");
				String history1 = action + " : " + m.date();
				//list.add(history1);


				continue;
			case "RIVER":
				River r = new River();
				//System.out.println(m.pwd()+ "\\river");
				String river="";
				int numb = 0;
				int numb1= 0;
				int space= 0;
				space =action.indexOf(" ");
				String numero="";
				String numero2="";
				rivTab= action.split(" ");

				try {
					if(rivTab[1].equalsIgnoreCase("--help")) {
						r.help();
						list.add(action);
					} else {
						numb= Integer.parseInt(rivTab[1]);
						numb1=  Integer.parseInt(rivTab[2]);
						int B = River.river(numb, numb1);
						if(B<0) {
							String history3 = action + " the first number " + numb + " the second number " + numb1
									+ " Result : " + "you've reached Integer max value" + " : " + m.date();
							list.add(history3);
						}else {
							String history3 = action + " the first number " + numb + " the second number " + numb1
									+ " Result : " + B + " : " + m.date();
							list.add(history3);
						}

					}

				}catch(Exception e){
//					String history3 = action + " : " + m.date();
//					list.add(history3);
					System.out.println("Ooops something went wrong, please check out your parameters for command river");

				}


				continue;


			case "ISPRIME":
				Isprime i = new Isprime();

				int space1= 0;
				space1 =action.indexOf(" ");
				double numero3=0;
				try {
					rivTab= action.split(" ");
					if(rivTab[1].equalsIgnoreCase("--help")) {
						i.help();
						list.add(action);
					}else {
						numero3= Double.parseDouble(rivTab[1]);
						String B =Isprime.Isprime(numero3);
						String history4 = action
								+ " Result : " + B + " : " + m.date();
						list.add(history4);
					}


				}catch(Exception e){
//					String history4 = action+" : " + m.date();
//					list.add(history4);
					System.out.println("Ooops something went wrong, please check out your parameter command for isprime");

				}

				continue;

			case "DIR":
				Dir d= new Dir();
				d.displayContent();
				String history5 = action +" : " + m.date();
				list.add(history5);
				continue;

			case "DIRNG":
				Dirng dn= new Dirng();
				dn.displayContent();
				String history6 = action +" : " + m.date();
				list.add(history6);
				continue;

			case "CD":
				Cd c= new Cd();

				if(rivTab.length >= 2) {
					for (int j = 2; j < rivTab.length; j++) {
						rivTab[1] = rivTab[1] + " " +rivTab[j];
					}
				}try {
					if(c.changeDirectory(rivTab[1])==false) {
						String history7 = action +" : " + m.date();
						list.add(history7);
					}
				} catch(Exception e) {
//					System.out.println("Command not found");
//					String history7 = action +" : " + m.date();
//					list.add(history7);
					System.out.println("Ooops something went wrong, please check out your parameter for CD");

				}
				
			
				continue;


			case "HISTORY":
				for (String string : list) {
					System.out.println(string);
				}
				continue;

			case "FIND":
				if(rivTab.length==3&&rivTab[1].equals("-starts")) {
					Find.findstart(rivTab[2]);
					String history8 = action +" : " + m.date();
					list.add(history8);
				}

				if(rivTab.length==3&&rivTab[1].equals("-ends")) {
					Find.findends(rivTab[2]);
					String history8 = action +" : " + m.date();
					list.add(history8);
				}

				if(rivTab.length>3&&rivTab[1].equals("-starts") && rivTab[3].equals("-ends")) {
					Find.findStartEnds(rivTab[2],rivTab[4]);
					String history8 = action +" : " + m.date();
					list.add(history8);
				}
				if(rivTab.length>3&&rivTab[1].equals("-ends") && rivTab[3].equals("-starts")) {
					Find.findStartEnds(rivTab[4],rivTab[2]);
					String history8 = action +" : " + m.date();
					list.add(history8);
				}
				if(rivTab.length==2) {
					Find.findContains(rivTab[1]);
					String history8 = action +" : " + m.date();
					list.add(history8);
				}
				if(rivTab.length==0) {
					System.out.println("Please add parameter to find a file");
				}
				

				continue;

			case "CAT":
				if(rivTab.length >= 2) {
					for (int j = 2; j < rivTab.length; j++) {
						rivTab[1] = rivTab[1] + " " +rivTab[j];
					}
				}
				try {
					Cat.catread(rivTab[1]);
				}
				catch(Exception e) {
//					System.out.println("please add a file name as parameter");
//					String history9 = action +" : " + m.date();
//					list.add(history9);
					System.out.println("Ooops something went wrong, please check out your parameter command for Cat");

				}
				String history9 = action +" : " + m.date();
				list.add(history9);

				continue;

			case "HISTCLEAR":
				//System.out.println(m.pwd());
				list.clear();
				continue;
				
			case "GETVARS":
				
					if(rivTab.length==2 && rivTab[1].equals("-env")) {
						System.err.println("						  ENVIRONMENT VARIABLES\n");
						Getvars.env();	
						String history20 = action +" : " + m.date();
						list.add(history20);
					}
					if(rivTab.length==2 && rivTab[1].equals("-prop")) {
						System.err.println("						     JVM PROPERTIES\n");
						Getvars.prop();	
						String history91 = action +" : " + m.date();
						list.add(history91);
					}
					if(rivTab.length==1) {
						System.err.println("						  ENVIRONMENT VARIABLES\n");
						Getvars.env();
						System.err.println("						     JVM PROPERTIES\n");
						Getvars.prop();	
						String history95 = action +" : " + m.date();
						list.add(history95);
					}
					
//				}catch(Exception e) {
//					System.out.println("Please check your parameter for Getvars");
//				}
				
				
				continue;

			case "QUIT":
				System.out.println();
				System.out.println(
						"####################################################################################################################");
				System.out.println(
						"####################################################################################################################");
				System.out.println();
				System.out.println();
				ASCIIArtGenerator.ART_SIZE_SMALL("   SHUTTING DOWN");
				return;

			case "COPY":
				//				if(rivTab.length > 3) {
				//					for (int j = 2; j <action.indexOf('.'); j++) {
				//						rivTab[1] = rivTab[1] + " " +rivTab[j];
				//					}
				//				}

				try {
					Copy.copyFile(rivTab[1], rivTab[2]);
				}
				catch(Exception e) {
//					System.out.println("please check your paramameters to copy a file");
//					String history11 = action +" : " + m.date();
//					list.add(history11);
					System.out.println("Ooops something went wrong, please check out your parameter for copy");

				}
				String history11 = action +" : " + m.date();
				list.add(history11);

				continue;

			case "EXIT":
				System.out.println();
				System.out.println(
						"####################################################################################################################");
				System.out.println(
						"####################################################################################################################");
				System.out.println();
				System.out.println();
				ASCIIArtGenerator.ART_SIZE_SMALL("   SHUTTING DOWN");
				return;

			default:
				System.out.println("command not found \n");
				continue;
			}
		}

	}


	public void help() {
		System.out.println("\n					Bonjour sur la console cdi");
		System.out.println("\nCAT             : Read the content of a file");
		System.out.println("\nCD              : Change Directory");
		System.out.println("\nCOPY            : Copy a file in the same directory");
		System.out.println("\nCRD             : Create directory");
		System.out.println("\nCRF             : Create file");
		System.out.println("\nDIR		: Display the content of the current repertory");
		System.out.println("\nEXIT		: Shut down the program");
		System.out.println("\nHELP 		: Display all the commands available");
		System.out.println("\nHISTORY		: Display a command");
		System.out.println("\nHISTCLEAR	: Clean a command");
		System.out.println("\nISPRIME		: Display if a number is prime or not");
		System.out.println("\nPWD 		: Display the line utility for printing the current working directory ");
		System.out.println("\nRIVER		: Display the first intersection of two digits\n\n");
		
	}

	@Override
	public String pwd() {
		String s = System.getProperty("user.dir");
		return s;
	}


	@Override
	public String date() {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss yyyy-MM-dd");
		String formatDateTime = now.format(formatter);
		return formatDateTime;
	}

}
