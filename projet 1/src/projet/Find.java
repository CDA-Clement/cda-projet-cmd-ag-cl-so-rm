package projet;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Find implements Methodspp {
	private static int j=0;

	public static void findAuxstart (File root, String s) {

		File[] list = root.listFiles();

		if (list == null) return;

		for ( File f : list ) {
			if ( f.isDirectory() ) {
				findAuxstart(f,s);
				//				System.out.println( "Dir:" + f.getAbsoluteFile() );
			}
			else {
				if(f.getName().startsWith(s)) {
					j++;
					System.out.println(f.getPath());
				}
			}

		}
	}

	public static void findAuxEnd (File root, String s) {

		File[] list = root.listFiles();

		if (list == null) return;

		for ( File f : list ) {
			if ( f.isDirectory() ) {
				findAuxEnd(f,s);
				//				System.out.println( "Dir:" + f.getAbsoluteFile() );
			}
			else {
				if(f.getName().endsWith(s)) {
					j++;
					System.out.println(f.getPath());
				}
			}

		}
	}

	public static void findAuxStartEnd (File root, String start, String end) {

		File[] list = root.listFiles();

		if (list == null) return;

		for ( File f : list ) {
			if ( f.isDirectory() ) {
				findAuxStartEnd(f,start, end);
				//			System.out.println( "Dir:" + f.getAbsoluteFile() );
			}
			else {
				if(f.getName().startsWith(start)&&f.getName().endsWith(end)) {
					j++;
					System.out.println(f.getPath());
				}
			}

		}
	}

	public static void findstart (String s) {
		File root = new File( Pwd.getS() );
		findAuxstart(root,s);
		if(j>1) {
			System.out.println(j+" files found");
		} else {
			System.out.println(j+" file found");
		}
		j=0;
	}

	public static void findends(String s) {
		File root = new File( Pwd.getS() );
		findAuxEnd(root,s);
		if(j>1) {
			System.out.println(j+" files found");
		} else {
			System.out.println(j+" file found");
		}
		j=0;
	}

	public static void findStartEnds(String start, String end) {
		File root = new File( Pwd.getS() );
		findAuxStartEnd(root,start, end);
		if(j>1) {
			System.out.println(j+" files found");
		} else {
			System.out.println(j+" file found");
		}
		j=0;
	}


	@Override
	public void help() {
		// TODO Auto-generated method stub

	}

	public String pwd() {
		String s = System.getProperty("user.dir");
		return s;
	}


	@Override
	public String date() {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss yyyy-MM-dd");
		String formatDateTime = now.format(formatter);
		return formatDateTime;
	}

	public static void setJ(int j) {
		Find.j = j;
	}

	public static void findContains(String param) {
		File root = new File( Pwd.getS() );
		findAuxContains(root,param);
		if(j>1) {
			System.out.println(j+" files found");
		} else {
			System.out.println(j+" file found");
		}
		j=0;
	}

	private static void findAuxContains(File root, String param) {
		File[] list = root.listFiles();

		if (list == null) return;

		for ( File f : list ) {
			if ( f.isDirectory() ) {
				findAuxContains(f,param);
				//				System.out.println( "Dir:" + f.getAbsoluteFile() );
			}
			else {
				if(f.getName().contains(param)) {
					j++;
					System.out.println(f.getPath());
				}
			}

		}

	}

}





